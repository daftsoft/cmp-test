SOURCES=$(wildcard *.c)
PROGS=$(patsubst %.c,%,$(SOURCES))
CFLAGS= -S -fomit-frame-pointer
#-masm=att -fverbose-asm -save-temps
#-Wa,-adhln -g -no-pie
#-masm=att -fverbose-asm -save-temps

#-masm=intel

all: $(PROGS)

%: %.c
	$(CC) $(CFLAGS) $<

clean:
	@echo Cleaning...
	@rm -rf *.s *~ a.out *.o

.PHONY: all clean
